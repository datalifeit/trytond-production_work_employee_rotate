datalife_production_work_employee_rotate
========================================

The production_work_employee_rotate module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-production_work_employee_rotate/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-production_work_employee_rotate)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
