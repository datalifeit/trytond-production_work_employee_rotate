# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from .work import (WorkCenter, WorkCenterEmployeeRotate,
    WorkCenterEmployeeRotateEdit, WorkCenterEmployeeRotateEditLine,
    WorkCenterEmployeeRotateStart, WorkCenterEmployeeRotateEditDate)


def register():
    Pool.register(
        WorkCenter,
        WorkCenterEmployeeRotateStart,
        WorkCenterEmployeeRotateEdit,
        WorkCenterEmployeeRotateEditLine,
        WorkCenterEmployeeRotateEditDate,
        module='production_work_employee_rotate', type_='model')
    Pool.register(
        WorkCenterEmployeeRotate,
        module='production_work_employee_rotate', type_='wizard')
