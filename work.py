# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import random

from dateutil.relativedelta import relativedelta
from trytond.model import ModelView
from trytond.model import fields
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval, Bool
from trytond.transaction import Transaction
from trytond.wizard import Button
from trytond.wizard import StateAction
from trytond.wizard import StateView
from trytond.wizard import Wizard

__all__ = ['WorkCenter', 'WorkCenterEmployeeRotate',
    'WorkCenterEmployeeRotateStart', 'WorkCenterEmployeeRotateEdit',
    'WorkCenterEmployeeRotateEditLine', 'WorkCenterEmployeeRotateEditDate']


class WorkCenter(metaclass=PoolMeta):
    __name__ = 'production.work.center'

    sequence = fields.Integer('Sequence')


class WorkCenterEmployeeRotate(Wizard):
    """Work Center employee rotate"""
    __name__ = 'production.work.center.employee_rotate'

    start = StateView('production.work.center.employee_rotate.start',
        'production_work_employee_rotate.employee_rotate_start_view_form',
        [Button('Cancel', 'end', 'tryton-cancel'),
         Button('Next', 'edit_', 'tryton-ok', default=True)])
    edit_ = StateView('production.work.center.employee_rotate.edit',
        'production_work_employee_rotate.employee_rotate_edit_view_form',
        [Button('Back', 'start', 'tryton-back'),
         Button('Cancel', 'end', 'tryton-cancel'),
         Button('OK', 'rotate', 'tryton-ok', default=True)])
    rotate = StateAction('production_work_employee.act_employee_work_center')

    def default_start(self, fields):
        Date_ = Pool().get('ir.date')
        return {
            'date_': Date_.today(),
            'mode': 'round-robin',
            'repeat_': ''
        }

    def default_edit_(self, fields):
        res = {
            'date_': self.start.date_,
            'end_date': getattr(self.start, 'end_date', None),
            'work_centers': []
        }

        with Transaction().set_context(date=self.start.date_):
            centers = self._get_work_centers_to_rotate()
            new_pos = self._get_new_positions(centers)
            for xx, center in enumerate(centers):
                wcenter = {
                    'work_center': center.id,
                    'init_employee': center.employee.id,
                    'dates': []
                }
                for stdate, edate in self._get_dates():
                    ii = int((stdate - self.start.date_).days) * self._get_repeated_pos()
                    wcenter['dates'].append({
                        'start_date': stdate,
                        'end_date': edate,
                        'rotated_employee': (centers[
                            (new_pos[xx] + ii) % len(centers)].employee.id
                            if centers[(new_pos[xx] + ii) % len(centers)].employee
                            else None)
                    })
                res['work_centers'].append(wcenter)
        return res

    def _get_work_centers_to_rotate(self):
        pool = Pool()
        Workcenter = pool.get('production.work.center')

        centers = Workcenter.search([
            ('parent', '=', Transaction().context['active_id']),
            ('sequence', '!=', None),
            ('many_employees', '=', False)],
            order=[('sequence', 'ASC')])
        return [c for c in centers if c.employee]

    def _get_repeated_pos(self):
        if self.start.mode == 'round-robin':
            return -self.start.positions
        return -1

    def _get_dates(self):
        if self.start.repeat_ == '':
            return [(self.start.date_, getattr(self.start, 'end_date', None))]
        if self.start.repeat_ == 'daily':
            values = int((self.start.end_date - self.start.date_).days) + 1
            return [(self.start.date_ + relativedelta(days=x),
                     self.start.date_ + relativedelta(days=x))
                for x in range(0, values)]
        return []

    def _get_new_positions(self, items):
        _len = len(items)
        values = []
        if self.start.mode == 'round-robin':
            values = [(x, (x - self.start.positions) % _len)
                for x in range(0, _len)]
        elif self.start.mode == 'random':
            values = [(x, _id) for x, _id in enumerate(
                random.sample(list(range(0, _len)), _len))]
        return dict(values)

    def do_rotate(self, action):
        pool = Pool()
        CenterEmployee = pool.get('company.employee-work.center')

        to_save = []
        for center in self.edit_.work_centers:
            for _date in center.dates:
                to_save.append(CenterEmployee(
                    work_center=center.work_center.id,
                    employee=_date.rotated_employee.id,
                    date=_date.start_date,
                    end_date=_date.end_date)
                )
        if to_save:
            CenterEmployee.save(to_save)

        return action, {'res_id': list(map(int, to_save))}

    def transition_rotate(self):
        return 'end'


class WorkCenterEmployeeRotateStart(ModelView):
    """Start Work Center employee rotation"""
    __name__ = 'production.work.center.employee_rotate.start'

    date_ = fields.Date('Date', required=True)
    end_date = fields.Date('End date',
        states={'required': Eval('repeat_') != ''},
        depends=['repeat_'])
    mode = fields.Selection([
        ('round-robin', 'Round robin'),
        ('random', 'Random')], 'Mode', required=True)
    positions = fields.Integer('Positions',
        states={'required': Eval('mode') == 'round-robin',
                'invisible': Eval('mode') != 'round-robin'},
        depends=['mode'])
    repeat_ = fields.Selection([
        ('', ''),
        ('daily', 'Daily')], 'Repeat')


class WorkCenterEmployeeRotateEdit(ModelView):
    """Edit Work Center employee rotation"""
    __name__ = 'production.work.center.employee_rotate.edit'

    date_ = fields.Date('Date', readonly=True)
    end_date = fields.Date('End date', readonly=True)
    work_centers = fields.One2Many('production.work.center.employee_rotate.edit_line',
        None, 'Work centers')


class WorkCenterEmployeeRotateEditLine(ModelView):
    """Edit line Work Center employee rotation"""
    __name__ = 'production.work.center.employee_rotate.edit_line'

    work_center = fields.Many2One('production.work.center', 'Work center',
        readonly=True)
    init_employee = fields.Many2One('company.employee', 'Initial employee',
        readonly=True)
    dates = fields.One2Many('production.work.center.employee_rotate.edit_line_date',
        None, 'Dates')


class WorkCenterEmployeeRotateEditDate(ModelView):
    """Edit line Work Center employee rotation"""
    __name__ = 'production.work.center.employee_rotate.edit_line_date'

    start_date = fields.Date('Date', readonly=True)
    end_date = fields.Date('End date', readonly=True)
    rotated_employee = fields.Many2One('company.employee', 'Rotated employee',
        required=True)
